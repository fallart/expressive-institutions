<?php
use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;
use App\Models;

return [
    // Provides application-wide services.
    // We recommend using fully-qualified class names whenever possible as
    // service names.
    'dependencies' => [
        // Use 'invokables' for constructor-less services, or services that do
        // not require arguments to the constructor. Map a service name to the
        // class name.
        'invokables' => [
            // Fully\Qualified\InterfaceName::class => Fully\Qualified\ClassName::class,
            Helper\ServerUrlHelper::class => Helper\ServerUrlHelper::class,
        ],
        // Use 'factories' for services provided by callbacks/factory classes.
        'factories' => [
            Application::class => ApplicationFactory::class,
            Helper\UrlHelper::class => Helper\UrlHelperFactory::class,
            Models\UserTable::class => App\Models\TableFactory::class,
            Models\UserInfoTable::class => App\Models\TableFactory::class,
            Models\CityTable::class => App\Models\TableFactory::class,
            Models\CountryTable::class => App\Models\TableFactory::class,
            Models\DegreeTable::class => App\Models\TableFactory::class,
            Models\InstitutionTypeTable::class => App\Models\TableFactory::class,
            Models\InstitutionTable::class => App\Models\TableFactory::class,
            Models\FacultyTable::class => App\Models\TableFactory::class,
            Models\LanguageTable::class => App\Models\TableFactory::class,
            Zend\Db\Adapter\Adapter::class => App\DatabaseAdapterFactory::class,
        ],
    ],
];
