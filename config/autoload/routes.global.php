<?php

return [
    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\FastRouteRouter::class,
            App\Action\PingAction::class => App\Action\PingAction::class,
        ],
        'factories' => [
            App\Action\HomePageAction::class => App\Action\ActionFactory::class,
            App\Action\Facebook\LoginAction::class => App\Action\ActionFactory::class,
            App\Action\Facebook\CallbackAction::class => App\Action\ActionFactory::class,
            App\Action\Vkontakte\LoginAction::class => App\Action\ActionFactory::class,
            App\Action\Vkontakte\CallbackAction::class => App\Action\ActionFactory::class,
        ],
    ],

    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => App\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.ping',
            'path' => '/api/ping',
            'middleware' => App\Action\PingAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'facebook.login',
            'path' => '/facebook/login',
            'middleware' => App\Action\Facebook\LoginAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'facebook.callback',
            'path' => '/facebook/callback',
            'middleware' => App\Action\Facebook\CallbackAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
        [
            'name' => 'vkontakte.login',
            'path' => '/vkontakte/login',
            'middleware' => App\Action\Vkontakte\LoginAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'vkontakte.callback',
            'path' => '/vkontakte/callback',
            'middleware' => App\Action\Vkontakte\CallbackAction::class,
            'allowed_methods' => ['GET', 'POST'],
        ],
    ],
];
