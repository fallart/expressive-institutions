<?php

namespace App\Action\Facebook;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Facebook\Facebook;

use Facebook\Exceptions\{
    FacebookResponseException,
    FacebookSDKException
};

class CallbackAction
{
    private $router;

    private $template;

    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {
        $this->router   = $router;
        $this->template = $template;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $message = '';
        $accessTokenValue = '';
        $header = '';
        $tokenMetadataValue = '';

        $fb = new Facebook([
            'app_id' => '1747161218832113', // Replace {app-id} with your app id
            'app_secret' => '0b0164df17c0cf57c7c0de570859c860',
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            $message .= 'Graph returned an error: ' . $e->getMessage();
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            $message .= 'Facebook SDK returned an error: ' . $e->getMessage();
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                $header = 'HTTP/1.0 401 Unauthorized';
                $message .= "Error: " . $helper->getError() . "\n";
                $message .= "Error Code: " . $helper->getErrorCode() . "\n";
                $message .= "Error Reason: " . $helper->getErrorReason() . "\n";
                $message .= "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                $header = 'HTTP/1.0 400 Bad Request';
                $message .= 'Bad request';
            }
        } else {
            $message .= '<h3>Access Token</h3>';

            $oAuth2Client = $fb->getOAuth2Client();
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
            $message .= '<h3>Metadata</h3>';
            $tokenMetadataValue = $tokenMetadata;

            $tokenMetadata->validateAppId('1747161218832113'); // Replace {app-id} with your app id
            $tokenMetadata->validateExpiration();
            $isLongLived = $accessToken->isLongLived();

            if (!$isLongLived) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (FacebookSDKException $e) {
                    $message .= "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                }

                $message .= '<h3>Long-lived</h3>';
                $accessTokenValue = $accessToken->getValue();
            }
        }



        return new HtmlResponse(
            $this->template
                ->render('app::facebook-callback', [
                    'message' => $message,
                    'accessTokenValue' => $accessTokenValue,
                    'tokenMetadataValue' => $tokenMetadataValue
                ])
        );
    }
}
