<?php

namespace App\Action\Facebook;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Facebook\Facebook;

class LoginAction
{
    private $router;

    private $template;

    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {
        $this->router   = $router;
        $this->template = $template;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $fb = new Facebook([
            'app_id' => '1747161218832113', // Replace {app-id} with your app id
            'app_secret' => '0b0164df17c0cf57c7c0de570859c860',
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $loginUrl = $helper->getLoginUrl('http://0.0.0.0:8080/facebook/callback', ['email']);

        return new HtmlResponse($this->template->render('app::facebook-login', ['loginUrl' => $loginUrl]));
    }
}
