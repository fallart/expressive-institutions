<?php

namespace App\Action;

use App\Models\CityTable;
use App\Models\CountryTable;
use App\Models\Degree;
use App\Models\DegreeTable;
use App\Models\FacultyTable;
use App\Models\InstitutionTable;
use App\Models\InstitutionTypeTable;
use App\Models\LanguageTable;
use App\Models\User;
use App\Models\UserInfoTable;
use App\Models\UserTable;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class HomePageAction
{
    private $userTable;
    private $template;

    public function __construct(
        LanguageTable $userTable,
        Template\TemplateRendererInterface $template = null)
    {
        $this->userTable = $userTable;
        $this->template = $template;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $data = [];

//        $data['users'] = $this->userTable->findAllWithInfos();

        var_dump($this->userTable->findAll()); exit;
        return new HtmlResponse($this->template->render('app::home-page', $data));
    }
}
