<?php

namespace App\Action\Vkontakte;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use VKontakteSdk\Options;
use VKontakteSdk\Vkontakte;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Curl\Curl;

class CallbackAction
{
    /**
     * @var Router\RouterInterface
     */
    private $router;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * CallbackAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     */
    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {
        $this->router   = $router;
        $this->template = $template;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $queryParams = $request->getQueryParams();

        if (isset($queryParams['code'])) {
            $vk = new Vkontakte(new Options([
                'clientId' => '5535814',
                'clientSecret' => 'cDAbaYmGIWGaOmYMyqQB',
                'redirectUri' => 'http://localhost:8080/vkontakte/callback',
                'code' => $queryParams['code']
            ]));

            $loginUrl = $vk->getReceivingAccessTokenUrl();

            $curl = new Curl();
            $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
            $curl->get($loginUrl);

            if ($curl->error) {
                return new HtmlResponse($this->template->render('app::vkontakte-login', ['loginUrl' => $loginUrl]));
            }

            $infoUrl = $vk->getInfoUrl($curl->response->access_token);
            $curl = new Curl();
            $curl->get($infoUrl);
            $resultInfo = reset($curl->response->response);


            $firstName = $resultInfo->first_name;
            $lastName = $resultInfo->last_name;
            $photoMin = $resultInfo->photo_50;
            $photoMax = $resultInfo->photo_200;



            return new HtmlResponse($this->template->render('app::vkontakte-info', [
                'firstName' => $firstName,
                'lastName' => $lastName,
                'photoMin' => $photoMin,
                'photoMax' => $photoMax,
            ]));
        }

        $message = $queryParams['error'] ?? '';
        $message .= $queryParams['error_description'] ?? '';

        return new HtmlResponse(
            $this->template
                ->render('app::vkontakte-callback', [
                    'message' => $message
                ])
        );
    }
}
