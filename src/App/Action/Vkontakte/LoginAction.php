<?php

namespace App\Action\Vkontakte;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use VKontakteSdk\Options;
use VKontakteSdk\Vkontakte;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;


class LoginAction
{
    /**
     * @var Router\RouterInterface
     */
    private $router;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * LoginAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     */
    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {
        $this->router   = $router;
        $this->template = $template;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $vk = new Vkontakte(new Options([
            'clientId' => '5535814',
            'clientSecret' => 'cDAbaYmGIWGaOmYMyqQB',
            'redirectUri' => 'http://localhost:8080/vkontakte/callback'
        ]));


        $loginUrl = $vk->getAuthorizationDialogUrl();
        return new HtmlResponse($this->template->render('app::vkontakte-login', ['loginUrl' => $loginUrl]));
    }
}
