<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 12/26/16
 * Time: 2:48 PM
 */

namespace App;

use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;
use Zend\Expressive\Router;
use App\Action;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencyConfig(),
            'routes' => $this->getRouteConfig(),
            'db' => $this->getDbConfig(),
        ];
    }

    private function getDependencyConfig()
    {
        return [
            'invokables' => [
                Action\PingAction::class => Action\PingAction::class,
            ],
            'factories' => [
                Application::class => ApplicationFactory::class,
                Helper\UrlHelper::class => Helper\UrlHelperFactory::class,
            ],
        ];
    }

    private function getRouteConfig()
    {
        return [
            'dependencies' => [
                'invokables' => [
                    Action\PingAction::class => Action\PingAction::class,
                ],
                'factories' => [
                    Action\HomePageAction::class => Action\ActionFactory::class,
                    Action\Facebook\LoginAction::class => Action\ActionFactory::class,
                    Action\Facebook\CallbackAction::class => Action\ActionFactory::class,
                    Action\Vkontakte\LoginAction::class => Action\ActionFactory::class,
                    Action\Vkontakte\CallbackAction::class => Action\ActionFactory::class,
                ],
            ],

            'routes' => [
                [
                    'name' => 'home',
                    'path' => '/',
                    'middleware' => Action\HomePageAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name' => 'api.ping',
                    'path' => '/api/ping',
                    'middleware' => Action\PingAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name' => 'facebook.login',
                    'path' => '/facebook/login',
                    'middleware' => Action\Facebook\LoginAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name' => 'facebook.callback',
                    'path' => '/facebook/callback',
                    'middleware' => Action\Facebook\CallbackAction::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
                [
                    'name' => 'vkontakte.login',
                    'path' => '/vkontakte/login',
                    'middleware' => Action\Vkontakte\LoginAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name' => 'vkontakte.callback',
                    'path' => '/vkontakte/callback',
                    'middleware' => Action\Vkontakte\CallbackAction::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    private function getDbConfig()
    {
        return [
            'driver'    => 'Mysqli',
            'host'      => '0.0.0.0:32768',
            'database'  => 'institutions',
            'username'  => 'root',
            'password'  => '3325',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
        ];
    }
}