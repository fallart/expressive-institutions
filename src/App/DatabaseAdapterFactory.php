<?php

namespace App;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\Adapter;

class DatabaseAdapterFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['db'];

        return new Adapter($config);
    }
}
