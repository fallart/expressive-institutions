<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:03 AM
 */

namespace App\Models;


/**
 * Class AbstractModel
 *
 * @package App\Models
 */
abstract class AbstractModel
{
    /**
     * @param array $data
     *
     * @return self
     */
    public abstract function populate(array $data);
}