<?php
namespace App\Models;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature\FeatureSet;

/**
 * Class AbstractTable
 *
 * @package App\Models
 */
abstract class AbstractTable extends AbstractTableGateway
{
    /** @var string  */
    protected $objectClass = '';

    /**
     * AbstractTable constructor.
     *
     * @param Adapter $adapter
     * @param FeatureSet|null  $featureSet
     */
    public function __construct(Adapter $adapter, FeatureSet $featureSet = null)
    {
        $this->adapter = $adapter;
        $this->featureSet = $featureSet;
        $this->initialize();
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function populate(array $data)
    {
        /** @var AbstractModel $object */
        $object = new $this->objectClass();

        return $object->populate($data);
    }

    /**
     * @param array|null $where
     *
     * @return array
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $data = $this->select($where);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id)
    {
        return reset($this->findAll(['id' => $id]));
    }
}
