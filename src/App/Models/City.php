<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/3/17
 * Time: 11:51 AM
 */

namespace App\Models;


/**
 * Class City
 *
 * @package App\Models
 */
class City extends AbstractModel
{

    /**
     * @var int
     */
    protected $id;
    /**
     * @var int
     */
    protected $geonameId;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $country;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return City
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getGeonameId(): int
    {
        return $this->geonameId;
    }

    /**
     * @param int $geonameId
     *
     * @return City
     */
    public function setGeonameId(int $geonameId)
    {
        $this->geonameId = $geonameId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return City
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return City
     */
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return City
     */
    public function populate(array $data): City
    {
        $countryFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                case 'geonameId':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                    $this->{$key} = $item;
                    break;
                case 'countryid':
                case 'countrygeonameId':
                case 'countryname':
                case 'countrycode':
                    $countryFields[mb_substr($key, 7)] = $item;
                    break;
            }
        }

        if (!empty($countryFields)) {
            $country = new Country();
            $this->country = $country->populate($countryFields);
        }

        return $this;
    }
}