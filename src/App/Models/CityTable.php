<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/3/17
 * Time: 11:51 AM
 */

namespace App\Models;


class CityTable extends AbstractTable
{

    const TABLE_NAME = 'cities';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = City::class;

    /**
     * @param array|null $where
     *
     * @return City[]
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                CountryTable::TABLE_NAME,
                self::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'countryid' => 'id',
                    'countrygeonameId' => 'geonameId',
                    'countryname' => 'name',
                    'countrycode' => 'code'
                ]
            );
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return City
     */
    public function find(int $id): City
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return City
     */
    protected function populate(array $data): City
    {
        return parent::populate($data);
    }


}