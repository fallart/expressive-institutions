<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:35 PM
 */

namespace App\Models;


class Country extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var int
     */
    protected $geonameId;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $code;

    /**
     * @param array $data
     *
     * @return self
     */
    public function populate(array $data)
    {
        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                case 'geonameId':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                case 'code':
                    $this->{$key} = $item;
                    break;
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Country
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getGeonameId(): int
    {
        return $this->geonameId;
    }

    /**
     * @param int $geonameId
     *
     * @return Country
     */
    public function setGeonameId(int $geonameId)
    {
        $this->geonameId = $geonameId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Country
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Country
     */
    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }
}