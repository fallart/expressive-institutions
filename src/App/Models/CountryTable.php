<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:36 PM
 */

namespace App\Models;


class CountryTable extends AbstractTable
{
    const TABLE_NAME = 'countries';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = Country::class;

    /**
     * @param array|null $where
     *
     * @return Country[]
     */
    public function findAll(?array $where = []): array
    {
        return parent::findAll($where);
    }

    /**
     * @param $id
     *
     * @return Country
     */
    public function find(int $id)
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return Country
     */
    protected function populate(array $data): Country
    {
        return parent::populate($data);
    }

}