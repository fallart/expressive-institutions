<?php
namespace App\Models;


/**
 * Class Degree
 *
 * @package App\Models
 */
class Degree extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $country;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Degree
     */
    public function setId(int $id): Degree
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Degree
     */
    public function setName(string $name): Degree
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return Degree
     */
    public function setCountry(string $country): Degree
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Degree
     */
    public function populate(array $data): Degree
    {
        $countryFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                    $this->{$key} = $item;
                    break;
                case 'countryid':
                case 'countrygeonameId':
                case 'countryname':
                case 'countrycode':
                    $countryFields[mb_substr($key, 7)] = $item;
                    break;
            }
        }

        if (!empty($countryFields)) {
            $country = new Country();
            $this->country = $country->populate($countryFields);
        }

        return $this;
    }
}