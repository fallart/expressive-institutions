<?php
namespace App\Models;


class DegreeTable extends AbstractTable
{

    const TABLE_NAME = 'degrees';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = Degree::class;

    /**
     * @param array|null $where
     *
     * @return Degree[]
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                CountryTable::TABLE_NAME,
                self::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'countryid' => 'id',
                    'countrygeonameId' => 'geonameId',
                    'countryname' => 'name',
                    'countrycode' => 'code'
                ]
            );
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return Degree
     */
    public function find(int $id): Degree
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return Degree
     */
    protected function populate(array $data): Degree
    {
        return parent::populate($data);
    }


}