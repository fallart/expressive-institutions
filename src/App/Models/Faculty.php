<?php
namespace App\Models;

/**
 * Class Faculty
 *
 * @package App\Models
 */
class Faculty extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var Institution
     */
    protected $institution;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Faculty
     */
    public function setName(string $name): Faculty
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return Faculty
     */
    public function setId(int $id): Faculty
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Faculty
     */
    public function populate(array $data): Faculty
    {
        $institutionFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                    $this->{$key} = $item;
                    break;
                case 'instid':
                case 'instname':
                case 'insttypeid':
                case 'insttypename':
                case 'instcityid':
                case 'instcitygeonameId':
                case 'instcityname':
                case 'instcitycountryid':
                case 'instcitycountrygeonameId':
                case 'instcitycountryname':
                case 'instcitycountrycode':
                    $institutionFields[mb_substr($key, 4)] = $item;
                    break;
            }
        }

        if (!empty($institutionFields)) {
            $institution = new Institution();
            $this->type = $institution->populate($institutionFields);
        }

        return $this;
    }
}