<?php

namespace App\Models;

/**
 * Class FacultyTable
 *
 * @package App\Models
 */
class FacultyTable extends AbstractTable
{
    const TABLE_NAME = 'faculties';
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = Faculty::class;

    /**
     * @param array $data
     *
     * @return Faculty
     */
    protected function populate(array $data): Faculty
    {
        return parent::populate($data);
    }

    /**
     * @param array|null $where
     *
     * @return Faculty[]
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                InstitutionTable::TABLE_NAME,
                self::TABLE_NAME . '.institutionId = ' . InstitutionTable::TABLE_NAME . '.id',
                [
                    'instid' => 'id',
                    'instname' => 'name',
                ]
            )
            ->join(
                InstitutionTypeTable::TABLE_NAME,
                InstitutionTable::TABLE_NAME . '.typeId = ' . InstitutionTypeTable::TABLE_NAME . '.id',
                [
                    'insttypeid' => 'id',
                    'insttypename' => 'name',
                ]
            )
            ->join(
                CityTable::TABLE_NAME,
                InstitutionTable::TABLE_NAME . '.cityId = ' . CityTable::TABLE_NAME . '.id',
                [
                    'instcityId' => 'id',
                    'instcityname' => 'name',
                ]
            )
            ->join(
                CountryTable::TABLE_NAME,
                CityTable::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'instcitycountryid' => 'id',
                    'instcitycountrygeonameId' => 'geonameId',
                    'instcitycountryname' => 'name',
                    'instcitycountrycode' => 'code'
                ]
            );
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return Faculty
     */
    public function find(int $id): Faculty
    {
        return reset($this->findAll(['id' => $id]));
    }
}