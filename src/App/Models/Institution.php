<?php
namespace App\Models;

/**
 * Class Institution
 *
 * @package App\Models
 */
class Institution extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var InstitutionType
     */
    protected $type;
    /**
     * @var City
     */
    protected $city;
    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return InstitutionType
     */
    public function getType(): InstitutionType
    {
        return $this->type;
    }

    /**
     * @param InstitutionType $type
     *
     * @return Institution
     */
    public function setType(InstitutionType $type): Institution
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param City $city
     *
     * @return Institution
     */
    public function setCity(City $city): Institution
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Institution
     */
    public function setName(string $name): Institution
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return Institution
     */
    public function setId(int $id): Institution
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Institution
     */
    public function populate(array $data): Institution
    {
        $typeFields = [];
        $cityFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                    $this->{$key} = $item;
                    break;
                case 'typeid':
                case 'typename':
                    $typeFields[mb_substr($key, 4)] = $item;
                    break;
                case 'cityid':
                case 'citygeonameId':
                case 'cityname':
                case 'citycountryid':
                case 'citycountrygeonameId':
                case 'citycountryname':
                case 'citycountrycode':
                $cityFields[mb_substr($key, 4)] = $item;
                    break;
            }
        }

        if (!empty($typeFields)) {
            $type = new InstitutionType();
            $this->type = $type->populate($typeFields);
        }

        if (!empty($cityFields)) {
            $city = new City();
            $this->city = $city->populate($cityFields);
        }

        return $this;
    }
}