<?php

namespace App\Models;

/**
 * Class InstitutionTable
 *
 * @package App\Models
 */
class InstitutionTable extends AbstractTable
{
    const TABLE_NAME = 'institutions';
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = Institution::class;

    /**
     * @param array $data
     *
     * @return Institution
     */
    protected function populate(array $data): Institution
    {
        return parent::populate($data);
    }

    /**
     * @param array|null $where
     *
     * @return Institution[]
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                InstitutionTypeTable::TABLE_NAME,
                self::TABLE_NAME . '.typeId = ' . InstitutionTypeTable::TABLE_NAME . '.id',
                [
                    'typeid' => 'id',
                    'typename' => 'name',
                ]
            )
            ->join(
                CityTable::TABLE_NAME,
                self::TABLE_NAME . '.cityId = ' . CityTable::TABLE_NAME . '.id',
                [
                    'cityId' => 'id',
                    'cityname' => 'name',
                ]
            )
            ->join(
                CountryTable::TABLE_NAME,
                CityTable::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'citycountryid' => 'id',
                    'citycountrygeonameId' => 'geonameId',
                    'citycountryname' => 'name',
                    'citycountrycode' => 'code'
                ]
            );

        var_dump($select->getSqlString());
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return Institution
     */
    public function find(int $id): Institution
    {
        return reset($this->findAll(['id' => $id]));
    }
}