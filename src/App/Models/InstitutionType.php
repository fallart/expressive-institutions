<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:35 PM
 */

namespace App\Models;


class InstitutionType extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * @param array $data
     *
     * @return self
     */
    public function populate(array $data)
    {
        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                    $this->{$key} = $item;
                    break;
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return InstitutionType
     */
    public function setId(int $id): InstitutionType
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return InstitutionType
     */
    public function setName(string $name): InstitutionType
    {
        $this->name = $name;

        return $this;
    }
}