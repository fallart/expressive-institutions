<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:36 PM
 */

namespace App\Models;


class InstitutionTypeTable extends AbstractTable
{
    const TABLE_NAME = 'institutionTypes';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = InstitutionType::class;

    /**
     * @param array|null $where
     *
     * @return InstitutionType[]
     */
    public function findAll(?array $where = []): array
    {
        return parent::findAll($where);
    }

    /**
     * @param $id
     *
     * @return InstitutionType
     */
    public function find(int $id)
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return InstitutionType
     */
    protected function populate(array $data): InstitutionType
    {
        return parent::populate($data);
    }

}