<?php
namespace App\Models;


class Language extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $code;

    /**
     * @param array $data
     *
     * @return self
     */
    public function populate(array $data): Language
    {
        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                case 'code':
                    $this->{$key} = $item;
                    break;
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): Language
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): Language
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode(string $code): Language
    {
        $this->code = $code;

        return $this;
    }
}