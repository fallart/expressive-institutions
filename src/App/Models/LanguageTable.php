<?php
namespace App\Models;


class LanguageTable extends AbstractTable
{
    const TABLE_NAME = 'languages';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = Language::class;

    /**
     * @param array|null $where
     *
     * @return Language[]
     */
    public function findAll(?array $where = []): array
    {
        return parent::findAll($where);
    }

    /**
     * @param $id
     *
     * @return Language
     */
    public function find(int $id): Language
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return Language
     */
    protected function populate(array $data): Language
    {
        return parent::populate($data);
    }

}