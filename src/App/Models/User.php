<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:03 AM
 */

namespace App\Models;


/**
 * Class User
 *
 * @package App\Models
 */
class User extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $authKey;
    /**
     * @var string
     */
    protected $passwordHash;
    /**
     * @var string
     */
    protected $passwordResetToken;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var int
     */
    protected $status;
    /**
     * @var UserInfo
     */
    protected $info;
    /**
     * @var \DateTime
     */
    protected $createdAt;
    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthKey(): string
    {
        return $this->authKey;
    }

    /**
     * @return UserInfo
     */
    public function getInfo(): UserInfo
    {
        return $this->info;
    }

    /**
     * @param UserInfo $info
     *
     * @return User
     */
    public function setInfo(UserInfo $info): User
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @return string
     */
    public function getPasswordResetToken(): string
    {
        return $this->passwordResetToken;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $authKey
     *
     * @return User
     */
    public function setAuthKey(string $authKey)
    {
        $this->authKey = $authKey;

        return $this;
    }

    /**
     * @param string $passwordHash
     *
     * @return User
     */
    public function setPasswordHash(string $passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * @param string $passwordResetToken
     *
     * @return User
     */
    public function setPasswordResetToken(string $passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param int $status
     *
     * @return User
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function populate(array $data)
    {
        $infoFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                case 'status':
                    $this->{$key} = (int)$item;
                    break;
                case 'authKey':
                case 'passwordHash':
                case 'passwordResetToken':
                case 'email':
                    $this->{$key} = $item;
                    break;
                case 'createdAt':
                case 'updatedAt':
                    $this->{$key} = new \DateTime($item);
                    break;
                case 'infoid':
                case 'infoname':
                case 'infosurname':
                case 'infocountryid':
                case 'infocountrygeonameId':
                case 'infocountryname':
                case 'infocountrycode':
                    $infoFields[mb_substr($key, 4)] = $item;
                    break;
            }
        }

        if (!empty($infoFields)) {
            $info = new UserInfo();
            $this->info = $info->populate($infoFields);
        }

        return $this;
    }
}