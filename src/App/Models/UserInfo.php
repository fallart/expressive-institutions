<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:30 PM
 */

namespace App\Models;


/**
 * Class UserInfo
 *
 * @package App\Models
 */
class UserInfo extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $surname;
    /**
     * @var Country
     */
    protected $country;

    /**
     * @param array $data
     *
     * @return self
     */
    public function populate(array $data)
    {
        $countryFields = [];

        foreach ($data as $key => $item) {

            switch ($key) {
                case 'id':
                    $this->{$key} = (int)$item;
                    break;
                case 'name':
                case 'surname':
                case 'country':
                    $this->{$key} = $item;
                    break;
                case 'countryid':
                case 'countrygeonameId':
                case 'countryname':
                case 'countrycode':
                    $countryFields[mb_substr($key, 7)] = $item;
                    break;
            }
        }

        if (!empty($countryFields)) {
            $country = new Country();
            $this->country = $country->populate($countryFields);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return UserInfo
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return UserInfo
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return UserInfo
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return UserInfo
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }
}