<?php
/**
 * Created by PhpStorm.
 * User: afaller
 * Date: 1/2/17
 * Time: 10:31 PM
 */

namespace App\Models;

class UserInfoTable extends AbstractTable
{
    const TABLE_NAME = 'userInfos';

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = UserInfo::class;

    /**
     * @param array $data
     *
     * @return UserInfo
     */
    protected function populate(array $data): UserInfo
    {
        return parent::populate($data);
    }

    /**
     * @param array|null $where
     *
     * @return UserInfo[]
     */
    public function findAll(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                CountryTable::TABLE_NAME,
                self::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'countryid' => 'id',
                    'countrygeonameId' => 'geonameId',
                    'countryname' => 'name',
                    'countrycode' => 'code'
                ]
            );
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    /**
     * @param $id
     *
     * @return UserInfo
     */
    public function find(int $id)
    {
        return parent::find($id);
    }
}