<?php

namespace App\Models;

/**
 * Class UserTable
 *
 * @package App\Models
 */
class UserTable extends AbstractTable
{
    const TABLE_NAME = 'users';
    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;
    /**
     * @var string
     */
    protected $objectClass = User::class;

    /**
     * @param array|null $where
     *
     * @return User[]
     */
    public function findAll(?array $where = []): array
    {
        return parent::findAll($where);
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function find(int $id): User
    {
        return parent::find($id);
    }

    /**
     * @param array $data
     *
     * @return User
     */
    protected function populate(array $data): User
    {
        return parent::populate($data);
    }


    public function findAllWithInfos(?array $where = []): array
    {
        $result = [];
        $select = $this->getSql()
            ->select()
            ->where($where)
            ->join(
                UserInfoTable::TABLE_NAME,
                self::TABLE_NAME . '.id = ' . UserInfoTable::TABLE_NAME . '.userId',
                [
                    'infoid' => 'id',
                    'infoname' => 'name',
                    'infosurname' => 'surname',
                ]
            )
            ->join(
                CountryTable::TABLE_NAME,
                UserInfoTable::TABLE_NAME . '.countryId = ' . CountryTable::TABLE_NAME . '.id',
                [
                    'infocountryid' => 'id',
                    'infocountrygeonameId' => 'geonameId',
                    'infocountryname' => 'name',
                    'infocountrycode' => 'code'
                ]
            );
        $data = $this->executeSelect($select);

        foreach ($data as $item) {
            $result[] = $this->populate($item->getArrayCopy());
        }

        return $result;
    }

    public function findWithInfos(int $id): User
    {
        return reset($this->findAllWithInfos(['id' => $id]));
    }
}